from django.db import models
from django.utils.text import slugify

import uuid

## generar migracion de la tabla 
## python manage.py makemigrations
## agregar migraciones pendientes
## python manage.py migrate

class Product(models.Model):
    titulo = models.CharField( max_length=50 )
    description = models.TextField()
    price = models.DecimalField( max_digits=8, decimal_places=2, default= 0.0 ) #123456.78
    slug =  models.SlugField( null=False, blank=False, unique=True )
    image = models.ImageField( upload_to='products/', null=False, blank=False )
    create_at = models.DateTimeField( auto_now_add=True )

    def __str__(self) -> str:
        return self.titulo

    def save( self, *args, **kwargs ):

        slug = slugify( self.titulo )        
        exist =Product.objects.filter(slug=slug).exists()

        if exist:
            self.slug = slugify( '{}-{}'.format( self.titulo, str( uuid.uuid4() )[:8] ) )
        else:
            self.slug = slug

        super( Product,self ).save( *args, **kwargs )