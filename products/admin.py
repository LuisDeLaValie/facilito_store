from django.contrib import admin
from .models import Product



class ProductAdmin(admin.ModelAdmin):
    fields = ('titulo', 'description', 'price','image')
    list_display = ('__str__', 'slug','create_at')


# Register your models here.
admin.site.register( Product, ProductAdmin )


