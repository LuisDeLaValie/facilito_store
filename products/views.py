from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

from django.db.models import Q

from products.models import Product



class ProductsListView(ListView):
    template_name = 'index.html'
    queryset = Product.objects.all().order_by('-id')

    def get_context_data( self, **kwargs ):
        contex =  super().get_context_data( **kwargs )
        contex['message'] = 'Listado de Productos'

        return contex


class ProductDetailView(DetailView):
    model = Product
    template_name = 'products/product.html'

    def get_context_data( self, **kwargs ):
        contex =  super().get_context_data( **kwargs )
        return contex


class ProductSearchListView(ListView):
    template_name = 'products/search.html'
    # queryset = 

    def get_queryset(self):        

        # titulo__icontains -> SELECT * FROM Products WHERE titulo LIKE '%%'
        filter1 = Q( titulo__icontains=self.query() )
        # categorias__titulo__icontains -> JOIN categoaria = productas WHERE categoaria.titulo LIKE '%%'
        filter2 = Q( categorias__titulo__icontains=self.query() )
        filters = filter1 | filter2

        return Product.objects.filter( filters )

    def query(self):
        return self.request.GET.get('q')
    
    def get_context_data( self, **kwargs ):
        contex =  super().get_context_data( **kwargs )
        contex['query'] = self.query()
        contex['count'] = contex['product_list'].count()

        return contex

