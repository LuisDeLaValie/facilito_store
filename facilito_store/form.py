from django import forms
from users.models import User

class RegisterForm(forms.Form):
    userName= forms.CharField( 
        required=True,
        min_length=4,
        max_length=50,
        widget=forms.TextInput(attrs={
        'class':"form-control",
        'id':"userName",
        'placeholder':"userName"
    }) )
    email= forms.EmailField(
        required=True,
        widget=forms.EmailInput(attrs={
        'class':"form-control",
        'id':"email",
        'placeholder':"email"
    }) )
    passw= forms.CharField( 
        required=True,
        widget=forms.PasswordInput(attrs={
        'class':"form-control",
        'id':"passw",
        'placeholder':"passw"
    }) )
    passw2= forms.CharField( 
        label="Confirmar Passsw",
        required=True,
        widget=forms.PasswordInput(attrs={
        'class':"form-control",
        'id':"passw2",
        'placeholder':"repetir paassw"
    }) )


    def clean_email( self ):
        username  = self.cleaned_data.get("email")

        if User.objects.filter(email=username).exists():
            raise forms.ValidationError('Email en Uso')

        return username

    def clean_userName( self ):
        username  = self.cleaned_data.get("userName")

        if User.objects.filter(username=username).exists():
            raise forms.ValidationError('Usuario en Existencia')

        return username

    def clean( self ):

        cleaned_data = super().clean()

        passw  = cleaned_data.get("passw")
        passw2  = cleaned_data.get("passw2")

        if passw != passw2 :
            self.add_error('passw2', 'El password no coincide')
        
    def save( self ):
        return User.objects.create_user(
            self.cleaned_data.get('userName'),
            self.cleaned_data.get('email'),
            self.cleaned_data.get('passw')
        )




