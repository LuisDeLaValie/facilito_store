from .form import RegisterForm
from django.shortcuts import redirect, render
from django.contrib.auth import authenticate,login as login_django, logout
from django.contrib import messages
from users.models import User
from products.models import Product

def index( request ):
    products = Product.objects.all().order_by('-id')

    return render( 
        request,
        'index.html',
        {
            'titulo':"Productos",
            'message':"Lista Productos",
            'productos': products
        }
    )

def login( request ):

    if request.user.is_authenticated:
        return redirect('index')

    if request.method == 'POST':
        name = request.POST.get("username")
        passw = request.POST.get("paswword")
        print(name)
        print(passw)

        user = authenticate(username=name, password=passw)
        
        if user is not None:
            login_django(request, user)
            messages.success( request, 'Binenvenido {}'.format(user.get_username()) )
            return redirect('index')
        else:
            messages.error( request, 'Usrio o contraseña no validos' )


    return render( 
        request,
        'user/login.html',
        {

        }
    )

def logout_view( request ):
    logout( request )
    messages.success( request, 'Sesion serrada con exito' )
    return redirect('login')

def registro_view( request ):

    if request.user.is_authenticated:
        return redirect('index')

    form = RegisterForm( request.POST or None )

    if request.method == 'POST' and form.is_valid():
        user =  form.save()

        if user is not None:
            login_django( request, user )
            messages.success( request, 'Usuario Creado')
            return redirect('index')


    return render(
        request,
        'user/registro.html',
        {
            'form':form
        }
    )