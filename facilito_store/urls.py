"""facilito_store URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls import include

from django.conf.urls.static import static
from django.conf import settings


from products.views import ProductsListView
from . import view

urlpatterns = [
    path( 'admin/', admin.site.urls ),
    path( '', ProductsListView.as_view(), name='index' ),
    path( 'user/logout', view.logout_view, name='logout' ),
    path( 'user/registro', view.registro_view, name='registro' ),
    path( 'user/login', view.login, name='login' ),
    
    path( 'products/', include( 'products.urls' ) ),
    path( 'carrito/', include( 'carts.urls' ) ),
    
]

if settings.DEBUG:
    urlpatterns += static( settings.MEDIA_URL, document_root=settings.MEDIA_ROOT )
