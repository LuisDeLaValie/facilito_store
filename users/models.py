from django.db import models

from django.contrib.auth.models import AbstractUser

## generar migracion de la tabla 
## python manage.py makemigrations
## agregar migraciones pendientes
## python manage.py migrate
class User(AbstractUser):
    
    def get_full_name( self ):
        return '{} {}'.format( self.first_name, self.last_name )
# Create your models here.
