from django.db import models


from products.models import Product
## generar migracion de la tabla 
## python manage.py makemigrations
## agregar migraciones pendientes
## python manage.py migrate

class Categorias(models.Model):
    titulo = models.CharField( max_length=50 )
    detalles = models.TextField()
    create_at = models.DateTimeField(auto_now_add=True)

    # relacion de muchos a muchos
    products =  models.ManyToManyField( Product, blank=True )

    def __str__(self) -> str:
        return self.titulo